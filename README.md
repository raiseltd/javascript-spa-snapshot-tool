# Node.js based Javascript SPA snapshot tool #

Install the required packages with:


```
#!bash

npm install
```

Edit the snapshot tool config to set your domains, and/or Mandrill API key. Then run the snapshot tool:


```
#!bash

node app.js
```

Or using your favourite "keep-alive" node script such as:


```
#!bash

forever app.js
```



Apache users, set up your website virtual host as follows:


```
#!bash

<VirtualHost *:80>
    DocumentRoot "/path/to/your/public/directory/"
    ServerName your.domain.tld

    # Turn on Proxy Requests
    ProxyRequests On

    # The rest of your virtual host rules go here...

    <Directory "/path/to/your/public/directory/">
        Require all granted
        RewriteEngine On

        # Proxy pass to a PhantomJS server
        # Realtime snapshot routing
        RewriteCond %{QUERY_STRING} ^_escaped_fragment_=/?(.*)$
        RewriteRule ^(.*)$ http://127.0.0.1:3000/$1 [QSA,P,L]

        # The rest of your directory specific rules go here...
    </Directory>
</VirtualHost>
```

This will forward any requests containing the `_escaped_fragment_` query string parameter to the snapshot tool.
