#! /usr/bin/env node

// Require some modules
var express = require('express'),
	phantom = require('phantom'),
	nodemailer = require('nodemailer'),
	fs = require('fs'),
	url = require('url'),
	path = require('path'),
	mkdirp = require('mkdirp'),
	mandrillTransport = require('nodemailer-mandrill-transport'),
	colors = require('colors');

// Define our express app
var app = express( );


/**
 * Configure our snapshot tool
 */
var config = {

	/**
	 * Our cache expiry time (in hours)
	 */
	cache_expiry: 72,


	/**
	 * Our site domain base config
	 */
	base: {
		protocol: 'https',
		domain: 'www.goraise.co.uk'
	},

	/**
	 * Email options
	 * Used for Mandrill error reporting
	 */
	email: {
		from: 'Web Snapshot Tool <snapshottool@goraise.co.uk>',
		to: 'gary@razorcreations.com'
	},


	/**
	 * File content pre/suffixes
	 * Should saved snapshot HTML files be pre/suffixed with any additional code
	 */
	html_wrappers: {
		/**
		 * Prefix your saved HTML snapshots
		 */
		prefix: '<!doctype><html>',

		/**
		 * Suffix your saved HTML snapshots
		 */
		suffix: '</html>'
	},


	/**
	 * Mandril settings
	 */
	mandrill: {
		api_key: '3QUDJO4ehMc7ugs309sK1w'
	}

};


// Create our nodemailer transporter to proxy SMTP requests
var transporter = nodemailer.createTransport( mandrillTransport({
	auth: {
		apiKey: config.mandrill.api_key
	}
}));


/**
 * Listen to all routes into index.html
 *
 */
app.get('*', function( req, res ) {
	// Create our base URL request
	var request_url = format_request_url( req );

	// Create our cache full pathname
	var full_pathname = format_full_pathname( request_url );

	// res.send('Hello World!');
	// Create a Phantom object
	phantom.create(
		// Crawl settings
		'--ignore-ssl-errors=true',
		'--ssl-protocol=tlsv1',

		// Successful callback
		function( ph ) {
			// Create a page object
			ph.createPage( function( page ) {
				// Logging
				console.log( ( "\n" + 'Snapshot requested: ' + request_url ).grey );

				// Check our cache
				check_snapshot_cache( res, ph, page, request_url, full_pathname );
			});
		});
}, function( err ) {
	console.log(err);
});


/**
 * function format_request_url
 * Returns a fully formatted request URL ready to be snapshotted
 *
 * @param object req
 */
function format_request_url( req ) {
	// Create our base URL
	var request_url = config.base.protocol + '://' + config.base.domain;
	
	// Do we have our _escaped_fragment_ query string?
	if( req.query._escaped_fragment_ ) {
		// Update our request URL
		request_url += req.query._escaped_fragment_;
	} else {
		// Update our request URL
		request_url += '/';
	}

	return request_url;
}


/**
 * function format_full_pathname
 * Returns a fully formatted full pathname ready for cache I/O
 *
 * @param string request_url
 */
function format_full_pathname( request_url ) {
	// Grab our URL parts
	var url_parts = url.parse( request_url );
	
	// Create our filename
	var filename = url_parts.pathname || '/index';

	// Homepage?
	if( filename == '/' ) {
		filename = '/index';
	}

	// Add our extension to our filename
	filename += '.html';

	// Our full pathname to save our cache to
	return path.join( __dirname, '/cache' + filename );
}


/**
 * function check_snapshot_cache
 * Checks to see if we have a cached version of our snapshot request
 *
 * @param object res
 * @param object ph
 * @param object page
 * @param string request_url
 * @param string full_pathname
 */
function check_snapshot_cache( res, ph, page, request_url, full_pathname ) {
	// Logging
	console.log( "\t" + '- Checking cache for snapshot'.grey );

	// Check our cache
	fs.exists( full_pathname, function( exists ) {
		// Does it exist?
		if( exists ) {
			// Yes, lets serve it
			console.log( "\t" + '- Cache exists'.green );

			// Read the file
			fs.readFile( full_pathname, 'utf8', function( err, data ) {
				// Something went wrong?
				if( err ) {
					console.log( "\t" + '- Could not read file from cache'.red );
					console.log( err );
					return console.log( err );
				}

				// Logging
				console.log( "\t" + '- Checking if cached snapshot is in date'.grey );

				// Check the age of this cached snapshot
				fs.stat( full_pathname, function( err, stats ) {
					// Grab the time our cached snapshot was created
					var created_at = new Date( stats.ctime ).getTime( );

					// Grab the difference between then and now
					var time_difference = ( ( new Date( ).getTime( ) - created_at ) / 60000 ) / 60;

					// If our hours difference is less than our cache expiry...
					if( time_difference < config.cache_expiry ) {
						// .. serve our cache
						// Logging
						console.log( "\t" + '- Cached snapshot has not expired, Snapshot served successfully'.green );

						if ( data.indexOf('<link rel="canonical" href="/oops">') != -1 ) {
							res.status(404).send( data );
						} else {
							res.send( data );
						}
						// We have the cached data, lets serve it

		        		// Kill our phantom process
		        		ph.exit( );
					} else {
						// Logging
						console.log( "\t" + '- Cached snapshot has expired, taking fresh snapshot'.red );

						// Fresh snapshot
						take_snapshot( res, ph, page, request_url, full_pathname );	
					}
				});
			});
		} else {
			// Doesn't exist
			// Logging
			console.log( "\t" + '- No cache, taking fresh snapshot'.red );
			console.log(request_url);
			console.log(full_pathname);
			// Fresh snapshot
			take_snapshot( res, ph, page, request_url, full_pathname );			
		}
	});
}


/**
 * function take_snapshot
 * Opens a page and takes a snapshot of it
 *
 * @param object res
 * @param object ph
 * @param object page
 * @param string request_url
 * @param string full_pathname
 */
function take_snapshot( res, ph, page, request_url, full_pathname ) {
	// Replace erroneous characters
	request_url = request_url.replace( '%2F', '/' );
	request_url = request_url.replace( '{{', '' );
	request_url = request_url.replace( '}}', '' );

	// Some page settings
	page.set( 'settings.userAgent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36' );
	page.set( 'settings.loadImages', true );
	page.set( 'settings.localToRemoteUrlAccessEnabled', false );
	page.set( 'settings.resourceTimeout', 10000 );
	page.set( 'settings.webSecurityEnabled', true );

	/**
	 * event onResourceRequested
	 * Event callback for when our page requests any kind of resource
	 *
	 */
	page.onResourceRequested(
		function( requestData, request, log_from_phantom_scope ) {
			// Get the hostname of the requested data's URL
			var hostname = requestData.url.split('//')[1];

			if ( typeof hostname != "undefined" ) {
				if ( hostname.indexOf('/') >= 0 ) {
					hostname = hostname.split('/')[0];
				}
				// Match against a few things we don't want to receive
				var match = hostname.match(/facebook.com/g) || hostname.match(/inspectlet.com/g) || hostname.match(/bootstrapcdn.com/g) || null;
			} else {
				// Logging
				log_from_phantom_scope( 'Hostname was undefined', 'yellow' );

				// Abort the request
				request.abort( );
			}

	  		// Did the resource come from one of our blocked domains?
			if( match != null ) {
				// Logging
				log_from_phantom_scope( 'Blocking resource from ' + hostname, 'yellow' );

				// Abort the request
				request.abort( );
			}
		},
		function( requestData ) { },
		log_from_phantom_scope
	);

	page.onResourceTimeout = function( e ) {
		console.log(e.errorCode);   // it'll probably be 408 
		console.log(e.errorString); // it'll probably be 'Network timeout on resource'
		console.log(e.url);         // the url whose request timed out
		ph.exit(1);
	}

	/**
	 * Begin our page request
	 */
	page.open( request_url, function( status ) {
		// Successful page load?
		if( status !== 'success' ) {
			// Call our failed navigation function
			failed_navigation( status, request_url );

			ph.exit( );
		} else {
			// Logging
			console.log( "\t" + '- Page loaded successfully'.green );
			
				// Retrieve the document title and HTML from the source
				page.evaluate( function( ) {
					var count = new Date();
					count = count.getTime();
					while ( document.body.className.match('metas-loaded') != 'metas-loaded' ) {
						var now = new Date();
						now = now.getTime();

						if ( now > ( count + ( 1000 * 10 ) ) ) {
							break;
						}
					}

					if ( document.body.className.match('metas-loaded') == 'metas-loaded') {
						// Grab the current HTML
						var original_html = document.getElementsByTagName('html')[0].outerHTML;

						// Create a new HTML object and set its inner HTML
						var html = document.createElement('html');
						html.innerHTML = original_html;

						// Grab script tags and script tags count
						var scripts = html.getElementsByTagName('script');
						var i = scripts.length;
						
						// Iterate script tags and remove them
						while( i-- ) {
							scripts[i].parentNode.removeChild( scripts[i] );
						}

						// Raise Specific
						// Fix our stupid body animation
						html.getElementsByTagName('body')[0].setAttribute('style', 'transition: none');

						return {
							html: html.innerHTML
						};
					} else {
						return {
							html: ''
						}
					}

				}, function( result ) {
		        	// Do we have HTML?
		        	if( result.html ) {
		        		// Convert the HTML
		        		var html = append_html_wrappers( result.html );

		        		// Write our snapshot to cache
		        		cache_snapshot( html, full_pathname );	        		

						// Logging
						console.log( "\t" + '- Snapshot served successfully'.green );

		        		/**
		        		 * Output our response to the search bot
		        		 */
		        		if ( html.indexOf('<link rel="canonical" href="/oops">') != -1 ) {
							res.status(404).send( result.html );
						} else {
							res.send( result.html );
						}
		        		
		        	} else {
		        		// Call our failed HTML lookup callback
		        		failed_html_lookup( request_url );

		        		var oops_url = config.base.protocol + '://' + config.base.domain + '/oops';
		        		var oops_pathname = format_full_pathname( oops_url );
		        		check_snapshot_cache( res, ph, page, oops_url, oops_pathname );
		        	}

		        	// Page has loaded
					ph.exit( );
				});
			// }, 100);
		}
	});
}


/**
 * function log_from_phantom_scope
 * Allows us to log to our console from within the scope of a phantom resource request
 *
 * @param string msg
 * @param string colour
 */
function log_from_phantom_scope( msg, colour ) {
	console.log( ( "\t" + '- ' + msg )[colour] );
}


/**
 * function failed_navigation
 * Phantom failed to open the page correctly
 *
 * @param string status
 * @param string request_url
 */
function failed_navigation( status, request_url ) {
	// Logging
	console.log( ( "\t" + '- Page failed to load with status: ' + status ).red );

	// Send email via our transporter created above
	transporter.sendMail({
	    from: config.email.from,
	    to: config.email.to,
	    subject: "\t" + '- Failed to load webpage: ' + status,
	    html: request_url
	}, mandrill_email_callback);
}


/**
 * function append_html_wrappers
 * Refactors our resulting HTML accordingly and returns it
 *
 * @param string html
 * @return string html
 */
function append_html_wrappers( html ) {
	return ( config.html_wrappers.prefix + html + config.html_wrappers.suffix );
}


/**
 * function cache_snapshot
 * Writes a newly created snapshot to our cache folder
 *
 * @param string html
 * @param string full_pathname
 */
function cache_snapshot( html, full_pathname ) {
	// Logging
	console.log( "\t" + '- Saving snapshot to cache'.grey );

	// Check our directory structure exists
	mkdirp( path.dirname( full_pathname ), function ( err ) {
		// If we receive an error
		if( err ) {
			console.log( "\t" + '- Could not create cache directory structure'.red );
			console.log( err );
			return console.log( err );
		}

		// Save our file
		fs.writeFile( full_pathname, html, {
			encoding: 'utf8',
			flag: 'w'
		},function( err ) {
			// Something went wrong?
			if( err ) {
				console.log( "\t" + '- Could not write HTML to cache'.red );
				console.log( err );
				return console.log( err );
			}

			// File was saved if we got this far
			console.log( "\t" + '- HTML saved to cache'.green );
		}); 
	})
}


/**
 * function failed_html_lookup
 * Our snapshot tool couldn't recover HTML from the page
 *
 * @param string request_url
 */
function failed_html_lookup( request_url ) {
	// Logging
	console.error( "\t" + '- No HTML found'.red );

	// Send email via our transporter created above
	// transporter.sendMail({
	//     from: config.email.from,
	//     to: config.email.to,
	//     subject: 'Failed to find webpage HTML',
	//     html: request_url
	// }, mandrill_email_callback);
}


/**
 * function mandrill_email_callback
 * Handles our Nodemailer req callback to Mandrill
 *
 * @param string error
 * @param object info
 */
function mandrill_email_callback( error, info ) {
	if( error ) {
		console.log( "\t" + '- Error sending email' );
		console.log( error );
	} else {
		console.log( "\t" + '- Email sent' );
	}
}


// Kick start the server on localhost:3000
var server = app.listen( 3000, 'localhost', function( ) {
	// Capture our host/port config
	var host = server.address( ).address;
	var port = server.address( ).port;

	// Logging
	console.log( 'Snapshot tool listening at http://%s:%s', host, port );
});
